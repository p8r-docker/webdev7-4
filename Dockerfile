FROM ubuntu:bionic

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && apt-get install -yq --no-install-recommends \
  apt-utils \
  curl \
  wget \
  git \
  mc \
  vim \
  nano \
  lynx

RUN apt -y install software-properties-common
RUN add-apt-repository -y ppa:ondrej/php

RUN apt-get install -y  \
      apache2 \
      php7.4 php7.4-cli php7.4-dev php7.4-common php-xdebug \
      php7.4-mysqli php7.4-xml php7.4-xmlrpc php7.4-curl php7.4-gd \
      php7.4-imagick php7.4-cli php7.4-dev php7.4-imap php7.4-mbstring \
      php7.4-opcache php7.4-soap php7.4-zip php7.4-intl php-zmq php7.4-sqlite3

RUN apt-get install -y sqlite3 \
  graphicsmagick \
  imagemagick \
  ca-certificates \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

EXPOSE 80 443 9000

WORKDIR /var/www/html

RUN rm index.html
COPY index.html /var/www/html/index.html
COPY phpinfo.php /var/www/html/phpinfo.php

# Update apache2.conf to include all config files from directory /etc/apach2/vhosts
RUN sed -i -e '$a\' -e '\n\nIncludeOptional /etc/apache2/vhosts/*.conf\n' /etc/apache2/apache2.conf

# Enable mod_rewrite
RUN a2enmod rewrite

# Update apach2/php.ini - append XDebug ini section from file xdebug.ini
COPY xdebug.ini /tmp/xdebug.ini
RUN sed -i '$r /tmp/xdebug.ini' /etc/php/7.4/apache2/php.ini
RUN rm /tmp/xdebug.ini

# Gracefully restart Apache
RUN apache2ctl graceful

HEALTHCHECK --interval=5s --timeout=3s --retries=3 CMD curl -f http://localhost || exit 1

CMD apachectl -D FOREGROUND

